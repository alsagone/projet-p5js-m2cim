# M2 CIM - Programmation Créative - Sketch Le Petit Poucet

- Guillaume RULLIAT
- Eric YOUNUS PEMBA
- Hakim ABDOUROIHAMANE

## Présentation :

Le projet petit poucet via la bibliothèque P5.js est un projet de groupe dont les membres sont : Hakim, Eric, Guillaume.
Notre projet était de refaire la scène de la forêt du conte du petit poucet. Un décor de forêt avec des arbres, un chemin, la nuit noire, la lune et des cailloux, ainsi que des étoiles dans le ciel.
Le petit poucet se déplace de gauche à droite et mange des cailloux lorsqu’il se déplace à droite, les recraches lorsqu’il se déplace à gauche.
Les cailloux sont sur le sol et disparaissent au contact du personnage.
Guillaume c’est occupé du décor et du côté graphique de celui-ci en raison de ses compétences en graphisme. Hakim c’est occupé de créer le personnage et son animation et Eric c’est occupé de la création des arbres, ils ont travaillé de concerts sur le code et l’interactivité du projet.

Il y a dans le décor 3 couches :
Un arrière-plan avec son ciel sombre et les étoiles blanche et la lune blanche et ses cratères beiges.
Un plan moyen avec l’herbe de couleur verte ainsi que les arbres dont le tronc est marron et le feuillage vert foncé.
Un premier plan avec le chemin, le personnage principal du sketch et puis les cailloux que le personnage va ramasser.
C’était assez difficile de faire un code synthétique avec autant d’éléments à mettre sur la scène, j’ai fait avec mes compétences, c’est-à-dire que j’ai fait un élément par un élément et cela m’as pris beaucoup de temps.

## Explications des Interactions

### Déplacement du Petit Poucet

Les utilisateurs peuvent déplacer le Petit Poucet horizontalement à l'aide des touches fléchées gauche et droite. Lorsque ces touches sont enfoncées, le Petit Poucet réagit en se déplaçant dans la direction correspondante. Cette interaction permet aux utilisateurs de guider le personnage tout au long de son aventure sur l'écran.

Si le Petit Poucet sort de l'écran, sa position est "remise à zéro" et les cailloux disséminés sont effacés.

### Collecte et lâcher des Cailloux

Le Petit Poucet peut collecter et lâcher des cailloux en marchant dessus. Lorsqu'il marche sur un caillou, il le collecte, ce qui est représenté par le caillou disparaissant. Lorsqu'il repasse sur l'emplacement où il a collecté un caillou, il le lâche, ce qui est symbolisé par la réapparition du caillou. Cette interaction permet aux utilisateurs de laisser des marques visuelles sur l'écran et d'interagir avec l'environnement du Petit Poucet.

### Interaction avec la souris

#### Chapeau

L'utilisateur peut cliquer sur le chapeau du Petit Poucet pour le faire changer de couleur.
Chaque clic déclenche une fonction qui détecte si la souris est à l'intérieur du triangle qui constitue le chapeau.

#### Lune

L'utilisateur peut changer la couleur de la Lune en passant la souris dessus.
À chaque mouvement de souris, une fonction est appelée pour détecter si le curseur de la souris est à l'intérieur de la Lune

## Problèmes rencontrés

### Coordonnées du corps du Petit Poucet 

L'une des principales difficultés rencontrées était la création des coordonnées du corps du Petit Poucet. Trouver les bonnes proportions pour chaque partie du corps tout en maintenant une apparence réaliste était un défi. L'utilisation de ratios a permis de simplifier ce processus, mais il a fallu de nombreuses itérations pour parvenir à un résultat satisfaisant.

### Animation du personnage

Animer le personnage du Petit Poucet pour qu'il marche de manière fluide était également un défi. L'animation nécessitait des ajustements précis des positions des jambes et une coordination avec les mouvements du personnage. La création d'une animation réaliste a pris du temps et des efforts.

### Intégration des cailloux

L'intégration des cailloux dans la scène a été un autre défi. Le code devait gérer la collecte et le lâcher des cailloux de manière précise en fonction des mouvements du Petit Poucet. La détection de collision entre le personnage et les cailloux a nécessité une programmation minutieuse pour garantir que l'interaction se déroulait correctement.

### Détection des interactions avec la souris

Les deux interactions avec la souris ont nécessité l'utilisation de raisonnements mathématiques "avancés" : détection de l'appartenance d'un point à un triangle, étant donné les coordonnées de ses trois points et la détection de l'appartenance d'un point à un cercle, étant donné les coordonnées de son centre et son rayon.

### Ajout d'un deuxième canvas

On s'est heurtés à un problème lors de l'ajout d'un canvas auxiliaire pour séparer le décor du Petit Poucet - le dessin du Petit Poucet était inexplicablement décalé et on n'a pas pu résoudre ce bug donc on a abandonné.

## Conclusion

En conclusion, le projet "Petit Poucet" réalisé avec P5.js a été une expérience enrichissante. Il nous a permis de créer une expérience interactive basée sur un conte classique en combinant des compétences en design et en programmation. Tout au long du développement, nous avons été confrontés à des défis stimulants qui nous ont poussés à repousser nos limites et à rechercher des solutions créatives.
La répartition efficace des tâches au sein de l'équipe a été essentielle pour le succès du projet, chaque membre apportant sa contribution unique. La création du personnage du Petit Poucet a été l'un des aspects les plus complexes du projet, en particulier la gestion des coordonnées du corps et de l'animation.
Au final, le projet "Petit Poucet" a permis à l'équipe de développer ses compétences en programmation et en design, tout en offrant une expérience interactive amusante aux utilisateurs. Le résultat final est satisfaisant, et nous espérons qu'il inspirera d'autres personnes à explorer les possibilités créatives de la programmation.
