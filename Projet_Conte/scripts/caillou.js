class Caillou {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  getX() {
    return this.x;
  }

  getY() {
    return this.y;
  }

  /*
   * Dessine les cailloux lâchés par le Petit Poucet
   * Par principe, il lâche les cailloux derrière lui et les ramasse s'il repasse dessus
   * Donc on n'a juste à vérifier si l'abscisse du caillou est inférieure ou égale à celle du Petit Poucet
   * Si oui, on dessine le caillou
   */
  dessiner(petitPoucetX) {
    const caillouX = this.getX();

    if (caillouX <= petitPoucetX) {
      fill(0, 0, 0);
      ellipse(caillouX, this.getY(), 10, 10);
    }

    return;
  }
}
