// Renvoie un entier aléatoire compris entre a et b (inclus)
const nombreAleatoire = (a, b) => {
  let min, max;
  if (a < b) {
    min = a;
    max = b;
  } else {
    min = b;
    max = a;
  }

  return Math.floor(Math.random() * (max - min) + min);
};

/* Détecte si un point est dans un triangle
 * via https://stackoverflow.com/a/2049593
 */
const sign = (p1, p2, p3) => {
  return (
    (p1.getX() - p3.getX()) * (p2.getY() - p3.getY()) -
    (p2.getX() - p3.getX()) * (p1.getY() - p3.getY())
  );
};

const estDansTriangle = (p, p1, p2, p3) => {
  let d1, d2, d3;
  let has_neg, has_pos;

  d1 = sign(p, p1, p2);
  d2 = sign(p, p2, p3);
  d3 = sign(p, p3, p1);

  has_neg = d1 < 0 || d2 < 0 || d3 < 0;
  has_pos = d1 > 0 || d2 > 0 || d3 > 0;

  return !(has_neg && has_pos);
};

/* Vérifie si un point appartient à un cercle étant donné
 * les coordonnées de son centre et son rayon
 */
const appartientCercle = (point, centre, rayon) => {
  const deltaX = abs(point.getX() - centre.getX());
  const deltaY = abs(point.getY() - centre.getY());
  return Math.pow(deltaX, 2) + Math.pow(deltaY, 2) < Math.pow(rayon, 2);
};

// Vérifie si x est compris entre a et b (inclus)

const estCompris = (x, a, b) => {
  let min, max;
  if (a < b) {
    min = a;
    max = b;
  } else {
    min = b;
    max = a;
  }

  return min <= x && x <= max;
};
