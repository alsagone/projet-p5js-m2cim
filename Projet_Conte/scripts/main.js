const [largeurCanvas, hauteurCanvas] = [400, 400];
let p;

let petitPoucetX = 25;
let petitPoucetY = 400;
let vitesse = 4;

let listeCailloux = [];

for (let i = 20; i < largeurCanvas; i += 20) {
  listeCailloux.push(new Caillou(i, 380));
}

let couleurChapeau = [255, 0, 0];
let lunePointee = false;

function setup() {
  createCanvas(largeurCanvas, hauteurCanvas);
  ellipseMode(CENTER);
  rectMode(CENTER);
}

function dessinerDecor() {
  //ciel
  fill(15, 5, 107);
  rect(0, 0, 400, 200);
  noStroke();

  //terre
  fill(176, 242, 182);
  rect(0, 200, 400, 200);
  noStroke();

  //Route de terre
  fill(215, 215, 190);
  rect(0, 300, 400, 200);
  noStroke();

  //lune
  const couleurLune = !lunePointee ? [232, 220, 202] : [97, 49, 224];
  fill(couleurLune[0], couleurLune[1], couleurLune[2]);
  ellipse(350, 46, 55, 55);

  //trou de lune
  const couleurTrouLune = !lunePointee ? [245, 245, 220] : [176, 224, 49];
  fill(couleurTrouLune[0], couleurTrouLune[1], couleurTrouLune[2]);
  ellipse(343, 38, 15, 15);
  ellipse(364, 60, 13, 13);
  ellipse(342, 60, 11, 11);
  ellipse(366, 38, 9, 9);
  ellipse(358, 47, 9, 9);

  //etoiles
  fill(255, 255, 255);
  ellipse(56, 46, 5, 5);
  ellipse(15, 28, 5, 5);
  ellipse(5, 18, 5, 5);
  ellipse(20, 6, 5, 5);
  ellipse(30, 28, 5, 5);
  ellipse(73, 16, 5, 5);
  ellipse(86, 46, 5, 5);
  ellipse(95, 28, 5, 5);
  ellipse(102, 18, 5, 5);
  ellipse(120, 6, 5, 5);
  ellipse(140, 28, 5, 5);
  ellipse(163, 16, 5, 5);
  ellipse(156, 46, 5, 5);
  ellipse(164, 28, 5, 5);
  ellipse(177, 18, 5, 5);
  ellipse(182, 6, 5, 5);
  ellipse(195, 28, 5, 5);
  ellipse(203, 16, 5, 5);
  ellipse(56, 56, 5, 5);
  ellipse(15, 38, 5, 5);
  ellipse(5, 28, 5, 5);
  ellipse(20, 16, 5, 5);
  ellipse(30, 38, 5, 5);
  ellipse(73, 26, 5, 5);
  ellipse(86, 56, 5, 5);
  ellipse(95, 38, 5, 5);
  ellipse(102, 28, 5, 5);
  ellipse(120, 16, 5, 5);
  ellipse(140, 38, 5, 5);
  ellipse(163, 26, 5, 5);
  ellipse(156, 56, 5, 5);
  ellipse(164, 38, 5, 5);
  ellipse(177, 28, 5, 5);
  ellipse(182, 16, 5, 5);
  ellipse(195, 38, 5, 5);
  ellipse(66, 56, 5, 5);
  ellipse(25, 58, 5, 5);
  ellipse(5, 58, 5, 5);
  ellipse(20, 46, 5, 5);
  ellipse(30, 48, 5, 5);
  ellipse(73, 56, 5, 5);
  ellipse(86, 76, 5, 5);
  ellipse(95, 88, 5, 5);
  ellipse(102, 98, 5, 5);
  ellipse(120, 76, 5, 5);
  ellipse(140, 48, 5, 5);
  ellipse(163, 66, 5, 5);
  ellipse(156, 96, 5, 5);
  ellipse(164, 118, 5, 5);
  ellipse(177, 138, 5, 5);
  ellipse(182, 116, 5, 5);
  ellipse(195, 128, 5, 5);
  ellipse(56, 156, 5, 5);
  ellipse(15, 138, 5, 5);
  ellipse(5, 128, 5, 5);
  ellipse(20, 116, 5, 5);
  ellipse(30, 138, 5, 5);
  ellipse(73, 126, 5, 5);
  ellipse(86, 156, 5, 5);
  ellipse(95, 138, 5, 5);
  ellipse(102, 128, 5, 5);
  ellipse(120, 116, 5, 5);
  ellipse(140, 138, 5, 5);
  ellipse(163, 126, 5, 5);
  ellipse(156, 156, 5, 5);
  ellipse(164, 138, 5, 5);
  ellipse(177, 128, 5, 5);
  ellipse(182, 116, 5, 5);
  ellipse(195, 138, 5, 5);
  ellipse(56, 46, 5, 5);
  ellipse(15, 28, 5, 5);
  ellipse(5, 18, 5, 5);
  ellipse(20, 26, 5, 5);
  ellipse(30, 28, 5, 5);
  ellipse(73, 16, 5, 5);
  ellipse(86, 46, 5, 5);
  ellipse(95, 28, 5, 5);
  ellipse(102, 18, 5, 5);
  ellipse(120, 26, 5, 5);
  ellipse(140, 28, 5, 5);
  ellipse(163, 16, 5, 5);
  ellipse(156, 46, 5, 5);
  ellipse(164, 28, 5, 5);
  ellipse(177, 18, 5, 5);
  ellipse(182, 26, 5, 5);
  ellipse(195, 28, 5, 5);
  ellipse(56, 56, 5, 5);
  ellipse(15, 38, 5, 5);
  ellipse(5, 28, 5, 5);
  ellipse(20, 16, 5, 5);
  ellipse(30, 38, 5, 5);
  ellipse(73, 26, 5, 5);
  ellipse(86, 56, 5, 5);
  ellipse(95, 38, 5, 5);
  ellipse(102, 28, 5, 5);
  ellipse(120, 16, 5, 5);
  ellipse(140, 38, 5, 5);
  ellipse(163, 26, 5, 5);
  ellipse(156, 56, 5, 5);
  ellipse(164, 38, 5, 5);
  ellipse(177, 28, 5, 5);
  ellipse(182, 16, 5, 5);
  ellipse(195, 38, 5, 5);
  ellipse(203, 26, 5, 5);
  ellipse(208, 56, 5, 5);
  ellipse(214, 38, 5, 5);
  ellipse(227, 28, 5, 5);
  ellipse(236, 16, 5, 5);
  ellipse(243, 38, 5, 5);
  ellipse(66, 56, 5, 5);
  ellipse(25, 58, 5, 5);
  ellipse(5, 58, 5, 5);
  ellipse(20, 46, 5, 5);
  ellipse(30, 48, 5, 5);
  ellipse(73, 56, 5, 5);
  ellipse(86, 76, 5, 5);
  ellipse(95, 88, 5, 5);
  ellipse(102, 98, 5, 5);
  ellipse(120, 76, 5, 5);
  ellipse(140, 48, 5, 5);
  ellipse(163, 66, 5, 5);
  ellipse(156, 96, 5, 5);
  ellipse(164, 18, 5, 5);
  ellipse(177, 38, 5, 5);
  ellipse(182, 16, 5, 5);
  ellipse(195, 28, 5, 5);
  ellipse(203, 16, 5, 5);
  ellipse(208, 46, 5, 5);
  ellipse(214, 28, 5, 5);
  ellipse(227, 18, 5, 5);
  ellipse(236, 16, 5, 5);
  ellipse(243, 28, 5, 5);
  ellipse(273, 28, 5, 5);
  ellipse(292, 40, 5, 5);
  ellipse(264, 53, 5, 5);
  ellipse(310, 12, 5, 5);
  ellipse(380, 17, 5, 5);
  ellipse(380, 77, 5, 5);
  ellipse(377, 120, 5, 5);
  ellipse(277, 132, 5, 5);
  ellipse(343, 133, 5, 5);
  ellipse(303, 113, 5, 5);
  ellipse(245, 118, 5, 5);
  ellipse(222, 144, 5, 5);
  ellipse(252, 147, 5, 5);
  ellipse(56, 56, 5, 5);
  ellipse(15, 38, 5, 5);
  ellipse(5, 82, 5, 5);
  ellipse(20, 16, 5, 5);
  ellipse(30, 38, 5, 5);
  ellipse(3, 26, 5, 5);
  ellipse(6, 56, 5, 5);
  ellipse(5, 38, 5, 5);
  ellipse(20, 28, 5, 5);
  ellipse(20, 16, 5, 5);
  ellipse(40, 38, 5, 5);
  ellipse(63, 26, 5, 5);
  ellipse(56, 56, 5, 5);
  ellipse(64, 38, 5, 5);
  ellipse(77, 28, 5, 5);
  ellipse(82, 16, 5, 5);
  ellipse(95, 38, 5, 5);
  ellipse(103, 26, 5, 5);
  ellipse(108, 56, 5, 5);
  ellipse(114, 38, 5, 5);
  ellipse(127, 28, 5, 5);
  ellipse(136, 16, 5, 5);
  ellipse(143, 38, 5, 5);
  ellipse(156, 26, 5, 5);
  ellipse(166, 56, 5, 5);
  ellipse(172, 38, 5, 5);
  ellipse(178, 28, 5, 5);
  ellipse(184, 16, 5, 5);
  ellipse(188, 38, 5, 5);
  ellipse(163, 26, 5, 5);
  ellipse(176, 58, 5, 5);
  ellipse(199, 38, 5, 5);
  ellipse(203, 28, 5, 5);
  ellipse(357, 96, 5, 5);
  ellipse(218, 68, 5, 5);
  ellipse(215, 76, 5, 5);
  ellipse(230, 86, 5, 5);
  ellipse(273, 98, 5, 5);
  ellipse(267, 78, 5, 5);
  ellipse(249, 86, 5, 5);
  ellipse(234, 98, 5, 5);
  ellipse(277, 76, 5, 5);
  ellipse(281, 86, 5, 5);
  ellipse(294, 98, 5, 5);
  ellipse(308, 68, 5, 5);
  ellipse(282, 76, 5, 5);
  ellipse(264, 98, 5, 5);
  ellipse(277, 86, 5, 5);
  ellipse(307, 96, 5, 5);
  ellipse(320, 68, 5, 5);
  ellipse(295, 76, 5, 5);
  ellipse(300, 86, 5, 5);
  ellipse(313, 98, 5, 5);
  ellipse(327, 78, 5, 5);
  ellipse(309, 166, 5, 5);
  ellipse(304, 148, 5, 5);
  ellipse(317, 166, 5, 5);
  ellipse(321, 156, 5, 5);
  ellipse(334, 178, 5, 5);
  ellipse(308, 168, 5, 5);
  ellipse(312, 146, 5, 5);
  ellipse(324, 168, 5, 5);
  ellipse(337, 156, 5, 5);
  ellipse(309, 166, 5, 5);
  ellipse(304, 148, 5, 5);
  ellipse(317, 166, 5, 5);
  ellipse(321, 156, 5, 5);
  ellipse(334, 178, 5, 5);
  ellipse(308, 168, 5, 5);
  ellipse(362, 146, 5, 5);
  ellipse(374, 168, 5, 5);
  ellipse(387, 156, 5, 5);
  ellipse(17, 166, 5, 5);
  ellipse(121, 156, 5, 5);
  ellipse(234, 178, 5, 5);
  ellipse(278, 168, 5, 5);
  ellipse(136, 174, 5, 5);
  ellipse(210, 95, 5, 5);
  ellipse(188, 85, 5, 5);
  ellipse(78, 105, 5, 5);
  ellipse(58, 86, 5, 5);
  ellipse(36, 93, 5, 5);
  ellipse(36, 183, 5, 5);
  ellipse(76, 177, 5, 5);
  ellipse(106, 184, 5, 5);
  ellipse(176, 182, 5, 5);
  ellipse(274, 188, 5, 5);
  ellipse(377, 183, 5, 5);
}

function dessinerCailloux() {
  for (const c of listeCailloux) {
    c.dessiner(petitPoucetX);
  }
}

function dessinerSapin() {
  new Sapin().dessiner();
}
function dessinerPetitPoucet() {
  p.dessiner();
}

// Déplace le Petit Poucet à gauche ou à droite en faisant attention à ce qu'il ne sorte pas du canvas
function deplacer(direction) {
  switch (direction) {
    case "gauche":
      petitPoucetX -= vitesse;
      break;

    case "droite":
      petitPoucetX += vitesse;
      break;

    default:
      break;
  }

  // Reset de la position du Petit Poucet s'il est hors canvas
  if (!estCompris(petitPoucetX, 0, 400)) {
    petitPoucetX = 10;
  }
  return;
}

function changerCouleurChapeau() {
  couleurChapeau = [
    nombreAleatoire(0, 255),
    nombreAleatoire(0, 255),
    nombreAleatoire(0, 255),
  ];
}

function mouseClicked() {
  if (p.detectionClicChapeau(mouseX, mouseY)) {
    changerCouleurChapeau();
  }
}

function mouseMoved() {
  //On vérifie si mouseX et mouseY sont dans la Lune

  const positionSouris = new Point(mouseX, mouseY);

  //cf fonction dessineDecor() pour les coordonnées de la Lune et le rayon (55)
  const centreLune = new Point(350, 46);
  lunePointee = appartientCercle(positionSouris, centreLune, 55);
}

function draw() {
  let direction = undefined;

  if (keyIsDown(LEFT_ARROW)) {
    direction = "gauche";
  } else if (keyIsDown(RIGHT_ARROW)) {
    direction = "droite";
  }

  deplacer(direction);

  clear();

  p = new PetitPoucet(hauteurCanvas, couleurChapeau);

  rectMode(CORNERS);
  dessinerDecor();

  rectMode(CENTER);
  dessinerSapin();
  dessinerPetitPoucet();
  dessinerCailloux();
}
