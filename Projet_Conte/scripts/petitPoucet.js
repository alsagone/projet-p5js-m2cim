class PetitPoucet {
  constructor(hauteurCanvas, couleurChapeau) {
    this.x = petitPoucetX;
    this.y = petitPoucetY;
    this.couleurChapeau = couleurChapeau;

    // Ratio de taille pour chacun des éléments (le corps fait 50% de la taille totale, les jambes 20% etc...)
    this.ratio = {
      corps: 0.5,
      jambes: 0.2,
      tete: 0.15,
      chapeau: 0.15,
    };

    //hauteur de la pointe du chapeau au sol
    this.hauteurTotale = hauteurCanvas * 0.5;

    this.coordonnees = {
      corps: undefined,
      jambes: undefined,
      tete: undefined,
      chapeau: undefined,
    };

    this.calculerCordonnees();

    this.largeurChapeau = this.ratio.tete * this.hauteurTotale;
    this.hauteurChapeau = this.hauteurTotale * this.ratio.chapeau;
    this.pointGaucheChapeau = new Point(
      this.coordonnees.tete.getX() - this.largeurChapeau / 2,
      this.coordonnees.tete.getY()
    );
    this.pointDroitChapeau = new Point(
      this.coordonnees.tete.getX() + this.largeurChapeau / 2,
      this.coordonnees.tete.getY()
    );
    this.pointHauteurChapeau = new Point(
      (this.pointGaucheChapeau.getX() + this.pointDroitChapeau.getX()) / 2,
      this.pointDroitChapeau.getY() - this.hauteurChapeau
    );
  }

  // Calcule toutes les coordonnées des points définissant le dessin du Petit Poucet
  calculerCordonnees() {
    const rayonJambes = ((this.ratio.jambes * this.hauteurTotale) / 4) >> 0;
    const rayonCorps = ((this.ratio.corps * this.hauteurTotale) / 4) >> 0;

    this.coordonnees.jambes = new Point(this.x, hauteurCanvas - rayonJambes);

    this.coordonnees.corps = new Point(
      this.x,
      this.coordonnees.jambes.getY() - rayonCorps - rayonJambes
    );

    const diametreTete =
      this.coordonnees.corps.getY() - this.ratio.tete * this.hauteurTotale;

    this.coordonnees.tete = new Point(this.x, diametreTete);

    this.coordonnees.chapeau = new Point(this.x, this.coordonnees.tete.getY());
  }

  dessinerTete() {
    fill(255, 255, 255);
    const rayon = this.ratio.tete * this.hauteurTotale;

    ellipse(
      this.coordonnees.tete.getX(),
      this.coordonnees.tete.getY(),
      rayon,
      rayon
    );
  }

  dessinerChapeau() {
    let [r, g, b] = this.couleurChapeau;
    fill(r, g, b);

    triangle(
      this.pointGaucheChapeau.getX(),
      this.pointGaucheChapeau.getY(),
      this.pointDroitChapeau.getX(),
      this.pointDroitChapeau.getY(),
      this.pointHauteurChapeau.getX(),
      this.pointHauteurChapeau.getY()
    );
  }

  dessinerCorps() {
    fill(195, 176, 144);
    rect(
      this.coordonnees.corps.getX(),
      this.coordonnees.corps.getY(),
      15,
      (this.ratio.corps * this.hauteurTotale) / 2,
      5
    );
  }

  dessinerJambes() {
    fill(255, 255, 255);
    rect(
      this.coordonnees.jambes.getX(),
      this.coordonnees.jambes.getY(),
      10,
      (this.ratio.jambes * this.hauteurTotale) / 2,
      5
    );
  }

  dessinerChaussures() {
    fill(0, 0, 0);
    ellipse(this.x, hauteurCanvas - 3, 5, 5);
  }

  dessiner() {
    this.dessinerCorps();
    this.dessinerChapeau();
    this.dessinerTete();
    this.dessinerJambes();
    this.dessinerChaussures();
  }

  // Détecte si les coordonnées du clic sont bien dans le chapeau
  detectionClicChapeau(clicX, clicY) {
    const pointClic = new Point(clicX, clicY);
    return estDansTriangle(
      pointClic,
      this.pointGaucheChapeau,
      this.pointDroitChapeau,
      this.pointHauteurChapeau
    );
  }
}
