class Point {
  constructor(abscisse, ordonnee) {
    this.x = abscisse;
    this.y = ordonnee;
  }

  getX() {
    return this.x;
  }

  setX(val) {
    this.x = val;
  }

  getY() {
    return this.y;
  }

  setY(val) {
    this.y = val;
  }
}

const mediane = (p1, p2) => {
  const medX = (p1.x + p2.x) / 2;
  const medY = (p1.y + p2.y) / 2;
  return new Point(medX, medY);
};
