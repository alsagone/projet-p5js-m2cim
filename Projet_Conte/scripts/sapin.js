class Sapin {
  constructor() {}

  dessiner() {
    // Tronc de l'arbre
    fill(139, 69, 19); // Marron
    rect(175, 300, 50, 80);

    // Dessiner les triangles pour les branches
    this.drawTriangle(200, 200, 200, 160); // Triangle du bas
    this.drawTriangle(200, 150, 180, 140); // 2e triangle
    this.drawTriangle(200, 100, 160, 120); // 3e triangle
    this.drawTriangle(200, 60, 140, 100); // 4e triangle (le plus haut)

    // Dessiner les boules de Noël
    fill(255, 255, 0);
    this.drawOrnament(190, 150);
    this.drawOrnament(220, 150);
    this.drawOrnament(180, 120);
    this.drawOrnament(210, 120);
    this.drawOrnament(195, 90);

    // Dessiner l'étoile
    fill(255, 255, 0);
    this.drawStar(200, 50, 15, 30, 5); // Étoile plus petite
  }

  drawTriangle(x, y, w, h) {
    fill(0, 128, 0); // Vert
    triangle(x, y, x - w / 2, y + h, x + w / 2, y + h);
  }

  drawOrnament(x, y) {
    fill(this.rEtoile, this.bEtoile, this.bEtoile);
    ellipse(x, y, 10, 10);
  }

  drawStar(x, y, radius1, radius2, npoints) {
    let angle = TWO_PI / npoints;
    let halfAngle = angle / 2.0;
    beginShape();
    for (let a = -PI / 2; a < TWO_PI - PI / 2; a += angle) {
      let sx = x + cos(a) * radius2;
      let sy = y + sin(a) * radius2;
      vertex(sx, sy);
      sx = x + cos(a + halfAngle) * radius1;
      sy = y + sin(a + halfAngle) * radius1;
      vertex(sx, sy);
    }
    endShape(CLOSE);
  }
}
