# M2 CIM - Programmation Créative - Jeu interactif

Hakim ABDOUROIHAMANE et Eric YOUNUS PEMBA

## Présentation

On a décidé de créer un jeu de coloriage par numéros.
Chaque case de la grille de dessin est numérotée et l'utilisateur doit choisir la couleur correspondante dans la palette en bas de l'écran et peindre la case en cliquant dessus.

## Explication des interactions

#### Boutons

L'utilisateur peut choisir entre trois dessins (un coeur, une abeille et un décor de plage).
Le bouton correspondant au dessin actuellement sélectionné est mis en surbrillance.

#### Score du joueur

Le pourcentage de progression du joueur dans le coloriage du dessin, à savoir le rapport entre le nombre de cases coloriées et le nombre total de cases, est indiqué en bas de la palette.

#### Fin de partie

À la fin de la partie, un message de victoire s'affiche accompagné d'un bouton permettant de recommencer de zéro le dessin en cours.

#### Navigation au clavier

L'utilisateur peut changer la couleur active de la palette en utiisant les flèches gauche/droite du clavier.
Les cases de la grille qui correspondent à la couleur active sont mises en surbrillance avec un arrière-plan gris.

## Difficultés rencontrées

La partie du développement qui a nécessité un peu plus de temps a été de trouver le bon placement de tous les objets dans le canvas et calculer les coordonnées adéquates pour que le jeu soit utilisable sur mobile.
On n'a malheureusement dû renoncer à intégrer un slider dans notre jeu parce qu'aucun élément ne s'y prêtait.
