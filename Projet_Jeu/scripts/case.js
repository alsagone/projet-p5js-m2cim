class Case {
  constructor(
    x,
    y,
    numeroCouleur,
    couleur,
    tailleCase,
    numeroCouleurSelectionnee = undefined
  ) {
    this.x = x;
    this.y = y;
    this.numeroCouleur = numeroCouleur;
    this.couleur = couleur;
    this.tailleCase = tailleCase;

    this.peinte = false;
    this.surbrillance = numeroCouleurSelectionnee === numeroCouleur;
  }

  dessiner() {
    // On dessine les bordures de la case si la case n'est pas peinte
    rectMode(CORNERS);

    if (!this.peinte) {
      //Si l'utilisateur a la bonne couleur sélectionée actuellement, remplir la case en gris
      if (this.surbrillance) {
        fill(204, 204, 204);
      } else {
        fill(255, 255, 255);
      }

      stroke(0, 0, 0);
      rect(this.x, this.y, this.x + this.tailleCase, this.y + this.tailleCase);

      //Dessin du numéro de la case
      textSize(this.tailleCase / 2);
      fill(0, 0, 0);
      noStroke();
      text(
        str(this.numeroCouleur),
        this.x + this.tailleCase / 2,
        this.y + this.tailleCase / 2
      );
    } else {
      //Pas de contour
      noStroke();

      //On remplit la case avec sa couleur
      fill(
        this.couleur.getRouge(),
        this.couleur.getVert(),
        this.couleur.getBleu()
      );
      rect(this.x, this.y, this.x + this.tailleCase, this.y + this.tailleCase);
    }
  }

  dessinerPalette() {
    //Dessin du contour de la case
    //On remplit la case avec sa couleur
    fill(
      this.couleur.getRouge(),
      this.couleur.getVert(),
      this.couleur.getBleu()
    );
    stroke(0, 0, 0);
    rect(this.x, this.y, this.x + this.tailleCase, this.y + this.tailleCase);

    //Dessin du numéro de la case
    textSize(this.tailleCase / 2);
    fill(255, 255, 255);
    noStroke();
    text(
      str(this.numeroCouleur),
      this.x + this.tailleCase / 2,
      this.y + this.tailleCase / 2
    );

    //Pas de contour
    noStroke();
  }

  // Vérifie si les coordonées du clic se trouvent dans la case  - avec une marge d'un pixel
  detectionClic(clicX, clicY) {
    return (
      estCompris(clicX, this.x, this.x + this.tailleCase) &&
      estCompris(clicY, this.y, this.y + this.tailleCase)
    );
  }

  setSurbrillance(b) {
    this.surbrillance = b;
    return;
  }
}
