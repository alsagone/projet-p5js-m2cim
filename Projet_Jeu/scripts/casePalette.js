class CasePalette {
  constructor(
    x,
    y,
    numeroCouleur,
    couleur,
    tailleCase,
    numeroCouleurSelectionnee
  ) {
    this.x = x;
    this.y = y;
    this.numeroCouleur = numeroCouleur;
    this.couleur = couleur;
    this.tailleCase = tailleCase;
    this.selectionnee = numeroCouleur == numeroCouleurSelectionnee;
  }

  dessinerPalette() {
    //Dessin du contour de la case
    //On remplit la case avec sa couleur
    fill(
      this.couleur.getRouge(),
      this.couleur.getVert(),
      this.couleur.getBleu()
    );

    if (this.selectionnee) {
      stroke(255, 0, 0);
      strokeWeight(4);
    } else {
      strokeWeight(1);
      stroke(0, 0, 0);
    }
    rect(this.x, this.y, this.x + this.tailleCase, this.y + this.tailleCase);

    //Dessin du numéro de la case
    textSize(this.tailleCase / 2);

    if (this.couleur.estBlanc()) {
      fill(0, 0, 0);
    } else {
      fill(255, 255, 255);
    }
    noStroke();
    text(
      str(this.numeroCouleur),
      this.x + this.tailleCase / 2,
      this.y + this.tailleCase / 2
    );
  }

  // Vérifie si les coordonées du clic se trouvent dans la case  - avec une marge d'un pixel
  detectionClic(clicX, clicY) {
    return (
      estCompris(clicX, this.x, this.x + this.tailleCase) &&
      estCompris(clicY, this.y, this.y + this.tailleCase)
    );
  }
}
