class Couleur {
  constructor(hexColor) {
    if (hexColor[0] != "#") {
      hexColor = "#" + hexColor;
    }

    this.rouge = parseInt(hexColor.slice(1, 3), 16);
    this.vert = parseInt(hexColor.slice(3, 5), 16);
    this.bleu = parseInt(hexColor.slice(5, 7), 16);
  }

  getRouge() {
    return this.rouge;
  }

  getVert() {
    return this.vert;
  }

  getBleu() {
    return this.bleu;
  }

  toHex() {
    const componentToHex = (c) => {
      const hex = c.toString(16);
      return hex.length == 1 ? "0" + hex : hex;
    };

    return (
      "#" + [this.rouge, this.vert, this.bleu].map(componentToHex).join("")
    );
  }

  estBlanc() {
    return this.rouge === 255 && this.vert === 255 && this.bleu === 255;
  }
}
