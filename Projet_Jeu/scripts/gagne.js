let message = "Bravo, Bien Joué !!!";
let fontSize = 32;
let snowflakes = [];

function setup() {
  createCanvas(400, 400);
  for (let i = 0; i < 50; i++) {
    snowflakes.push(new Snowflake());
  }
  text(173, 255, 47);
  textAlign(CENTER, CENTER);
}

function draw() {
  background(220);

  for (let flake of snowflakes) {
    flake.update();
    flake.display();
  }

  fill(0);
  textSize(fontSize);
  text(message, width / 2, height / 2);
}
