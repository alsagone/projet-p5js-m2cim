// Vérifie si x est compris entre a et b (inclus)

const estCompris = (x, a, b) => {
  let min, max;
  if (a < b) {
    min = a;
    max = b;
  } else {
    min = b;
    max = a;
  }

  return min <= x && x <= max;
};
