// Utilise les variables dans variablesGlobales.js

function touchStarted() {
  var fs = fullscreen();
  if (!fs) {
    fullscreen(true);
  }
}
/* full screening will change the size of the canvas */
function windowResized() {
  largeurCanvas = windowWidth;
  resizeCanvas(windowWidth, windowHeight);
}
/* prevents the mobile browser from processing some default
* touch events, like swiping left for "back" or scrolling the
page.
*/
document.ontouchmove = function (event) {
  event.preventDefault();
};

const creerCases = () => {
  listeCases = [];
  let couleurCase = undefined;
  let numeroCouleur = undefined;
  let maCase;

  for (let i = 0; i < nbCases; i++) {
    for (let j = 0; j < nbCases; j++) {
      // getPixelAt(j,i) pour avoir le dessin dans le bon sens
      numeroCouleur = dessin.getPixelAt(j, i);

      // La 1ère couleur de la palette c'est la couleur indice 0 dans le tableau
      couleurCase = dessin.getCouleurAt(Math.max(0, numeroCouleur - 1));

      maCase = new Case(
        i * tailleCase,
        j * tailleCase,
        numeroCouleur,
        couleurCase,
        tailleCase,
        numeroCouleurSelectionnee
      );

      listeCases.push(maCase);
    }
  }

  return;
};

const creerPalette = () => {
  let x = 10,
    y = (nbCases + 1) * tailleCase,
    p;

  palette = [];

  for (let i = 0; i < listeCouleurs.length; i++) {
    p = new CasePalette(
      x,
      y,
      i + dessin.debutPalette,
      listeCouleurs[i],
      tailleCase - 5,
      numeroCouleurSelectionnee
    );
    p.peinte = true;
    palette.push(p);
    x += (tailleCase - 5) * 1.5;
  }

  return;
};

const setDessin = (numeroDessin) => {
  changementEnCours = true;
  clear();
  dessinSelectionne = numeroDessin;
  dessin = listeDessins[numeroDessin - 1];
  nbCases = dessin.pixels[0].length;
  tailleCase = Math.floor(largeurCanvas / nbCases);
  listeCouleurs = dessin.couleurs;
  numeroCouleurSelectionnee = dessin.debutPalette;

  creerPalette();
  creerCases();

  changementEnCours = false;
};

const dessinerBoutons = () => {
  const couleurSurbrillance = color(41, 128, 185);
  const blanc = color(255, 255, 255);

  const y = nbCases * tailleCase + 200;
  textAlign(CENTER);

  //Si les boutons existent déjà, on les supprime
  if (btnCoeur) {
    btnCoeur.remove();
    btnAbeille.remove();
    btnPlage.remove();
  }

  btnCoeur = createButton("Coeur");
  btnCoeur.position(0, y);
  btnCoeur.mousePressed(() => setDessin(1));

  btnAbeille = createButton("Abeille");
  btnAbeille.position(80, y);
  btnAbeille.mousePressed(() => setDessin(2));

  btnPlage = createButton("Plage");
  btnPlage.position(160, y);
  btnPlage.mousePressed(() => setDessin(3));

  switch (dessinSelectionne) {
    case 1:
      btnCoeur.style("background-color", couleurSurbrillance);
      btnCoeur.style("color", blanc);
      break;

    case 2:
      btnAbeille.style("background-color", couleurSurbrillance);
      btnAbeille.style("color", blanc);
      break;

    case 3:
      btnPlage.style("background-color", couleurSurbrillance);
      btnPlage.style("color", blanc);
      break;

    default:
      break;
  }
};

const getProgression = () => {
  let nbCasesPeintes = 0;
  let totalCases = 0;

  for (const maCase of listeCases) {
    nbCasesPeintes += maCase.peinte ? 1 : 0;
    totalCases++;
  }

  return Math.trunc((nbCasesPeintes * 100) / totalCases);
};

const checkFini = () => {
  let b = true;

  for (const maCase of listeCases) {
    if (!maCase.peinte) {
      b = false;
      break;
    }
  }
  return b;
};

const creerBoutonReset = () => {
  const couleurSurbrillance = color(41, 128, 185);
  const blanc = color(255, 255, 255);

  if (btnReset) {
    btnReset.remove();
  }

  btnReset = createButton("Recommencer");
  btnReset.style("background-color", couleurSurbrillance);
  btnReset.style("color", blanc);
  btnReset.position(largeurCanvas / 3, largeurCanvas / 2 + 200);
  btnReset.mousePressed(() => setDessin(dessinSelectionne));
};

function setup() {
  createCanvas(windowWidth, windowHeight);
  ellipseMode(CENTER);
  rectMode(CENTER);
  largeurCanvas = windowWidth;
  setDessin(1);
}

function draw() {
  //Pour attendre la fin de la fonction setDessin()
  if (changementEnCours) {
    return;
  }

  clear();
  dessinerBoutons();

  if (!checkFini()) {
    //On supprime le bouton Recommencer s'il existe
    if (btnReset) {
      btnReset.remove();
    }

    for (const c of listeCases) {
      c.dessiner();
    }

    creerPalette();
    for (const c of palette) {
      c.dessinerPalette();
    }

    //Affichage de la progression totale
    textSize(20);
    fill(0, 0, 0);
    noStroke();
    text(
      `Progression totale: ${getProgression()} %`,
      largeurCanvas / 2,
      Math.floor(nbCases * tailleCase + 100)
    );
  } else {
    clear();
    fill(255, 255, 255);
    textSize(20);
    text(
      "Bravo vous avez fini ce dessin !",
      largeurCanvas / 2,
      largeurCanvas / 2
    );
    creerBoutonReset();
  }
}

const actualiserSurbrillanceCases = () => {
  let estEnSurbrillance;

  listeCases.forEach((maCase) => {
    estEnSurbrillance = numeroCouleurSelectionnee === maCase.numeroCouleur;
    maCase.setSurbrillance(estEnSurbrillance);
  });
};

function mousePressed() {
  //On balaie toutes les cases de la grille et si le clic est sur une de ces cases, on fait des trucs
  for (const c of listeCases) {
    if (
      c.detectionClic(mouseX, mouseY) &&
      numeroCouleurSelectionnee === c.numeroCouleur
    ) {
      c.peinte = true;
      return;
    }
  }

  for (const c of palette) {
    if (c.detectionClic(mouseX, mouseY)) {
      numeroCouleurSelectionnee = c.numeroCouleur;
      actualiserSurbrillanceCases();
      return;
    }
  }

  return;
}

function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    numeroCouleurSelectionnee = Math.max(1, numeroCouleurSelectionnee - 1);
    actualiserSurbrillanceCases();
  } else if (keyCode === RIGHT_ARROW) {
    numeroCouleurSelectionnee = Math.min(
      numeroCouleurSelectionnee + 1,
      palette.length
    );
    actualiserSurbrillanceCases();
  }
}
