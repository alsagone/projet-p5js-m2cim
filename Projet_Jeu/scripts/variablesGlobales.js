const listeDessins = [dessinCoeur, dessinAbeille, dessinPlage];

// Contient l'objet Dessin avec la liste des cases et les couleurs qui vont avec
let dessin = undefined;

// La liste des couleurs du dessin
let listeCouleurs = undefined;

// La largeur/hauteur du dessin (le dessin est carré)
let nbCases = undefined;

// Taille de la case en pixels
let tailleCase = undefined;

// La liste des cases du dessin
let listeCases = undefined;

// La palette
let palette = undefined;

// La largeur du canvas, initialisée dans la fonction setup()
let largeurCanvas = undefined;

// Le numéro de la couleur sélectionnée, réactualisé à chaque changement de couleur
let numeroCouleurSelectionnee = undefined;

//Numéro du dessins sélectionné (1, 2 ou 3)
let dessinSelectionne = 1;

// Booléen qui sert à bloquer le rafraîchissement du canvas (draw()) tant que la fonction setDessin() n'a pas fini
let changementEnCours = false;

// Boutons de changement de dessin
let btnCoeur = undefined,
  btnAbeille = undefined,
  btnPlage = undefined,
  btnReset = undefined;
